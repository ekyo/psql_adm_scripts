#!/bin/bash
#
# pg_backup_all_db.sh
# sauvegarde des bases postgresql, et gestion de la retention des backups
#
# git@git.framasoft.org:ekyo/psql_adm_scripts.git

export backup_dir="$(dirname $0)/backups_psql/"
retention=7
script_username="postgres"

# tests pre-execution
if ( ! echo ${script_username} | grep $(whoami) > /dev/null ) ; then
  echo -e "\n ERREUR : Ce script doit s'executer en tant que ${script_username}\n" && exit 1
fi

if [ ! -d ${backup_dir} ]; then
  mkdir -p ${backup_dir}
fi

###################################################################################
# Debut de traitement
echo -e "\n[$(date)] Debut du traitement\n"

# Sauvegarde des roles
pg_dumpall -v --globals-only  > ${backup_dir}/roles_`date +"%Y%m%d_%H%M"`.sql

# Sauvegarde des bases
for bdd in $(psql -l -t -U postgres | egrep -v "^ ( |template[01])" | awk '{print $1}'); do
  export backup_file="${backup_dir}${bdd}_$(date +%Y%m%d%H%M).dump.bz2"
  echo -n " -> Sauvegarde de la base $bdd vers ${backup_file}"
  pg_dump -Fc $bdd -U postgres | bzip2 -c > ${backup_file} && echo " `ls -lh ${backup_file}| awk '{ print $5 }'` OK" ||( echo " KO" && exit 1 )
done

# On nettoie
if [ $(find $backup_dir -type f -prune -mtime +${retention} | wc -l) -ne 0 ] ; then
  echo -e "\n -> Suppression de tous les backups > ${retention} jours "
  for tmpfile in $(find $backup_dir -type f -prune -mtime +${retention}) ; do
    echo "-> Suppression du fichier $tmpfile" && rm -f $tmpfile
  done
fi

# Fin de traitement
echo -e "\n[`date`] Fin du traitement\n"
####################################################################################
exit 0
