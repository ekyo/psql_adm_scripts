#!/bin/bash
#
# pg_get_size.sh
# genere un rapport avec la taille des tables, tables toast et indexes
#
# git@git.framasoft.org:ekyo/psql_adm_scripts.git

script_user="postgres"
logdir=$(dirname $0)/logs
logfile="${logdir}/pg_get_size_${base}_$(date +"%Y%m%d").log"

# tests pre-execution
[[ -z $1 ]] && echo "ERREUR : vous devez renseigner le nom de la database en parametre du script" && exit 1 || base=$1
[[ $(whoami) != ${script_user} ]] && echo "ERREUR : ce script doit s'executer en tant que ${script_user}" && exit 1
[[ ! -d ${logdir} ]] && mkdir -p ${logdir}

###################################################################################
# Debut de traitement
exec > ${logfile}
echo "$(date +"%Y%m%d %H:%M:%S") ##### Debut du traitement"

# Taille de la base

echo "======================================="
echo -n "Taille de la database ${base} : " ; psql -d ${base} -c "SELECT pg_size_pretty(pg_database_size('${base}'));"| tail -n +3 | head -n -2
echo "======================================="

# Liste et taille des tables (ainsi que de leurs tables toast et indexes associes)

for table in $(psql -d ${base} -c "select tablename from pg_tables where (tablename not like 'pg_%') and (tablename not like 'sql%' ) order by tablename" | tail -n +3 | head -n -2)
  do echo -n "${table} : " ;  psql -d ${base} -c "select pg_size_pretty(tablesize+indexsize+toastsize+toastindexsize) FROM (SELECT pg_relation_size(cl.oid) AS tablesize, COALESCE((SELECT SUM(pg_relation_size(indexrelid))::bigint FROM pg_index WHERE cl.oid=indrelid), 0) AS indexsize, CASE WHEN reltoastrelid=0 THEN 0 ELSE pg_relation_size(reltoastrelid) END AS toastsize, CASE WHEN reltoastrelid=0 THEN 0 ELSE pg_relation_size((SELECT reltoastidxid FROM pg_class ct WHERE ct.oid = cl.reltoastrelid)) END AS toastindexsize FROM pg_class cl WHERE relname = '${table}') ss" | tail -n +3 | head -n -2
done
# Fin de traitement
echo "$(date +"%Y%m%d %H:%M:%S") Fin du traitement #######"
####################################################################################
exit 0

