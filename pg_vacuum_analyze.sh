#!/bin/bash
#
# pg_vacuum_analyze.sh
# lance un vaccum analyze verbose
#
# git@git.framasoft.org:ekyo/psql_adm_scripts.git

script_user=postgres
logfile="$(dirname $0)/pg_vacuum_analyze_$(date +"%Y%m%d").log"

if [ `whoami` != $script_user ]
  then echo "ERREUR : ce script doit s'executer en tant que $script_user" ; exit 1
fi

echo "`date +"%Y%m%d %H:%M:%S"` ##### Debut du traitement" >> $logfile

for database in $(psql -l -t -U postgres | egrep -v "^ ( |template[01])" | awk '{print $1}'); do
    T="$(date +%s%N)"
    psql -o /dev/null -U postgres -d $database -c "SET maintenance_work_mem = 262144; VACUUM ANALYZE VERBOSE;" 2>&1 | tee -a ${logfile}
    T="$(($(date +%s%N)-T))"
    S="$((T/1000000000))"
    M="$((T/1000000))"
    exec_time=`printf "Temps d'execution : %02d:%02d:%02d.%03d\n" "$((S/3600%24))" "$((S/60%60))" "$((S%60))" "${M}"`
    echo "`date +"%Y%m%d"` VACUUM ANALYZE VERBOSE ${database} $exec_time" | tee -a $logfile
done

echo "`date +"%Y%m%d %H:%M:%S"` Fin du traitement #######" >> $logfile

