#!/bin/bash
#
# pg_reindex.sh
# reindexation de relations predefinies avec un intervalle defini lui aussi
# utilise en entree $fichier_in
#
# git@git.framasoft.org:ekyo/psql_adm_scripts.git

script_user=postgres
logfile="$(dirname $0)/pg_reindex_$(date +"%Y%m%d").log"
fichier_in="$(dirname $0)/pg_reindex.in"

# tests pre-execution
if [ $(whoami) != ${script_user} ]
  then echo "ERREUR : ce script doit s'executer en tant que ${script_user}" ; exit 1
fi

if [ ! -f ${fichier_in} ]
  then echo "ERREUR : le fichier d'entree ${fichier_in} n'existe pas" ; exit 1
fi

###################################################################################
# Debut de traitement
echo "$(date +"%Y%m%d %H:%M:%S") ##### Debut du traitement" >> ${logfile}

while read line ; do
  # decoupage de $fichier_in et affectation des variables
  set -- $(echo $line | awk -F ";" '{print $1, $2, $3, $4}')
  freq=${1}
  database=${2}
  type_obj=${3}
  relation=${4}

  # mise en forme des dates qui serviront a comparer
  last_reindex_date=$(tac ${logfile} | awk '/OK/ && /'${relation}'/ {print $1;exit;}')
  freq_date_compare=$(date -d "${freq} days ago" +"%Y%m%d")

  # initialisation a 0 si l'objet n'a jamais ete indexe ou n'est pas present dans le log
  if [ -z ${last_reindex_date} ]
    then last_reindex_date="00000000"
  fi

  # si la log indique une date indexation >= ( date - freq ) alors on reindex
  if [ ${freq_date_compare} -ge ${last_reindex_date} ] ; then
    T="$(date +%s%N)"
    if ( psql -o /dev/null -U postgres -d ${database} -c "SET maintenance_work_mem = 262144; REINDEX ${type_obj} ${relation}"; )
      then retval=OK ; else retval=ERREUR
    fi
    T="$(($(date +%s%N)-T))"
    S="$((T/1000000000))"
    M="$((T/1000000))"
    exec_time=`printf "Temps d'execution : %02d:%02d:%02d.%03d\n" "$((S/3600%24))" "$((S/60%60))" "$((S%60))" "${M}"`
    echo "`date +"%Y%m%d"` REINDEX ${type_obj} ${relation} ${retval} ${exec_time}" | tee -a ${logfile}
  fi

done < ${fichier_in}

# reindex des tables systeme
reindexdb -s | tee -a ${logfile}

# si retval n'a pas ete initialisee c'est qu'aucune action n'a ete menee
if [ -z ${retval} ]
  then echo "`date +"%Y%m%d"` Aucune action effectuee" | tee -a ${logfile}
fi

# Fin de traitement
echo "`date +"%Y%m%d %H:%M:%S"` Fin du traitement #######" >> ${logfile}
####################################################################################
exit 0
